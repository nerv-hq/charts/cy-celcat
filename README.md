# CY-Celcat

## External secrets

| KEY                  | Description     |
| -------------------- | --------------- |
| `CY_CELCAT_USER`     | Celcat username |
| `CY_CELCAT_PASSWORD` | Celcat password |
